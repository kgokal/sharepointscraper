import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.chilkatsoft.CkHttp;

public class ScraperMain {

	static {
		try {
			String workingDir = System.getProperty("user.dir");
			System.load(workingDir + "/lib/libchilkat.jnilib");
		} catch (UnsatisfiedLinkError e) {
			System.err.println("Native code library failed to load.\n" + e);
			System.exit(1);
		}
	}

	public static void main(String argv[]) throws Exception {
		checkArgs(argv);
		
		CkHttp http = createHttpRequest(argv);

		int total = 5000;
		int batch = 500;

		try {

			File file = new File("SharePoint Migration.csv");

			// if file doesnt exists, then create it
			if (!file.exists()) {
				file.createNewFile();
			}

			FileWriter fw = new FileWriter(file.getAbsoluteFile());
			BufferedWriter bw = new BufferedWriter(fw);


			for (int i = 0; i < total / batch; i++) {
				String url = "https://connect.daugherty.com/sites/teams/branches/atl/_api/search/query?querytext='sp07'&startrow=" + i * batch + "&rowlimit=" + batch;
				String response = http.quickGetStr(url);
				JSONArray results = getResults(response);
				processResults(results, bw);
			}

			bw.close();
			System.out.println("Done");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static void checkArgs(String[] argv) {
		if(argv == null || argv.length < 3 ) {
			System.out.println("USAGE: ScraperMain <domain> <user> <pass>");
			System.exit(-1);
		}
	}

	private static void processResults(JSONArray results, BufferedWriter bw) throws IOException {
		for (Object obj : results) {

			JSONArray subResults = getSubResults(obj);

			// for(Object rObj: subResults) {
			// System.out.println("KEY: " + ((JSONObject) rObj).get("Key") +
			// ", VALUE:" + ((JSONObject) rObj).get("Value"));
			// }

			JSONObject titleElement = (JSONObject) subResults.get(3);
			JSONObject pathElement = (JSONObject) subResults.get(6);
			JSONObject parentElement = (JSONObject) subResults.get(19);

			String client = (String) parentElement.get("Value");
			String[] cols = client.split("/", 8);

			// System.out.println("{title:" + titleElement.get("Value") +
			// ",path:'" + pathElement.get("Value") + "'}");//JSON
			bw.write("\"" + cols[6] + "\",\"" + titleElement.get("Value") + "\",\"" + pathElement.get("Value") + "\"");// CSV
			bw.newLine();
		}
	}

	private static JSONArray getSubResults(Object obj) {
		JSONObject jObj = (JSONObject) obj;
		JSONObject cells = (JSONObject) jObj.get("Cells");
		JSONArray subResults = (JSONArray) cells.get("results");
		return subResults;
	}

	private static JSONArray getResults(String response) throws ParseException {
		JSONParser parser = new JSONParser();
		JSONObject json = (JSONObject) parser.parse(response);
		JSONObject d = (JSONObject) json.get("d");
		JSONObject query = (JSONObject) d.get("query");
		JSONObject primary = (JSONObject) query.get("PrimaryQueryResult");
		JSONObject relavant = (JSONObject) primary.get("RelevantResults");
		JSONObject table = (JSONObject) relavant.get("Table");
		JSONObject rows = (JSONObject) table.get("Rows");
		JSONArray results = (JSONArray) rows.get("results");
		return results;
	}

	private static CkHttp createHttpRequest(String[] args) {
		CkHttp http = new CkHttp();

		boolean success;

		// Any string unlocks the component for the 1st 30-days.
		success = http.UnlockComponent("Anything for 30-day trial");
		if (success != true) {
			System.out.println(http.lastErrorText());
			System.exit(-1);
		}

		// Set the login/password for HTTP NTLM Authentication:
		http.put_Login(args[1]);
		http.put_Password(args[2]);
		http.put_LoginDomain(args[0]);
		// Negotiate authentication allows the HTTP client
		// to dynamically select either Kerberos or NTLM, depending
		// on what the server supports.
		http.put_NegotiateAuth(true);
		http.put_Accept("application/json;odata=verbose;charset=utf-8");
		return http;
	}
}
